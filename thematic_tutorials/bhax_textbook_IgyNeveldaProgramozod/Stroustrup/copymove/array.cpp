#include <iostream>

class Array {
    public:
        int* arr;
        int size;

    public:
        Array() {            //default constructor
            std::cout<<"Default constructor called.\n";
            arr = nullptr;
            size = 0;
        }

        Array(int elems) {   //constructor with given size
            std::cout<<"Constructor with given size called.\n";
            arr = new int[elems];
            size = elems;
        }

        Array(const Array& old) {   //copy constructor
            std::cout<<"Copy constructor called.\n";
            arr = new int[old.size];
            size = old.size;
            std::copy(old.arr, old.arr+old.size, arr);
        }

        Array& operator=(const Array& old) {  //copy assignment
            std::cout<<"Copy assignment called.\n";
            Array tmp = old;
            std::swap(*this, tmp);
            return *this;
        }

        Array(Array&& old) {     //move constructor
            std::cout<<"Move constructor called.\n";
            arr = nullptr;
            *this = std::move(old);
        }

        Array& operator=(Array&& old) {  //move assignment
            std::cout<<"Move assignment called.\n";
            std::swap(arr, old.arr);
            size = old.size;
            old.size = 0;
            return *this;
        }

        ~Array() {           //destructor
            std::cout<<"Destructor called.\n";
            delete[] arr;
        }

        void foo() {}
};

int main() {
    Array arr1(std::rand()%10);
    Array arr2 = arr1;
    Array arr3;
    arr3 = arr2;
    Array arr4 = std::move(arr3);
    Array arr5;
    arr5 = std::move(arr4);
}