#include <iostream>

class Array {
    public:
        int* arr;
        int size;

    public:
        Array() {            //default constructor
            std::cout<<"Default constructor called.\n";
            arr = nullptr;
            size = 0;
        }

        Array(int elems) {   //constructor with given size
            std::cout<<"Constructor with given size called.\n";
            arr = new int[elems];
            size = elems;
        }
        Array(const Array& old) {   //copy constructor
            std::cout<<"Copy constructor called.\n";
            arr = new int[old.size];
            size = old.size;
            std::copy(old.arr, old.arr+old.size, arr);
        }

        Array& operator=(const Array& old) {  //copy assignment
            std::cout<<"Copy assignment called.\n";
            delete[] arr;
            arr = new int[old.size];
            size = old.size;
            std::copy(old.arr, old.arr+old.size, arr);
            return *this;
        }

        Array(Array&& old) {     //move constructor
            std::cout<<"Move constructor called.\n";
            arr = old.arr;
            size = old.size;
            old.arr = nullptr;
            old.size = 0;
        }

        Array& operator=(Array&& old) {  //move assignment
            std::cout<<"Move assignment called.\n";
            delete[] arr;
            arr = old.arr;
            size = old.size;
            old.arr = nullptr;
            old.size = 0;
            return *this;
        }

        ~Array() {           //destructor
            std::cout<<"Destructor called.\n";
            delete[] arr;
        }

        void setRandArray(int size) {
            srand(time(NULL));
            for (int i=0; i<size; i++)
            {
                arr[i] = rand()%10;
            }
        }
};

int main() {
    Array arr1(3);
    arr1.setRandArray(arr1.size);
    Array arr2 = arr1;
    Array arr3;
    arr3 = arr2;

    std::cout<<"The values after copying: \n";
    for (int i=0; i<arr1.size; i++)
    {
        std::cout<<arr1.arr[i]<<"\t"<<arr2.arr[i]<<"\t"<<arr3.arr[i]<<"\n";
    }
    std::cout<<"\n";

    Array arr4 = std::move(arr3);
    Array arr5;
    arr5 = std::move(arr4);

    std::cout<<"The values after moving: \n";
    for (int i=0; i<arr1.size; i++)
    {
        std::cout<<arr5.arr[i]<<"\t";
    }
    std::cout<<"\n"<<arr4.arr<<"\n"<<arr3.arr<<"\n";
    std::cout<<"\n";
}
