#include <exception>
#include <string>
using namespace std;

#ifndef __Vehicle_h__
#define __Vehicle_h__

class Vehicle;

class Vehicle
{
	protected: string _model;
	protected: int _passengerCapacity;
	protected: int _age;
	/**
	 * km/h
	 */
	protected: double _maxSpeed;

	public: string getModel();

	public: void setModel(string aModel);

	public: int getCapacity();

	public: void setCapacity(int aCapacity);

	public: int getAge();

	public: void setAge(int aAge);

	public: double getSpeed();

	public: void setSpeed(double aSpeed);

	public: void makeSound();
};

#endif
