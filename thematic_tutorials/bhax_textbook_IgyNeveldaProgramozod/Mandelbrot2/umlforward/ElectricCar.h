#include <exception>
using namespace std;

#ifndef __ElectricCar_h__
#define __ElectricCar_h__

#include "Car.h"

// class Car;
class ElectricCar;

class ElectricCar: public Car
{
	private: double _batterySize;

	public: double getBattery();

	public: void setBattery(double aSize);
};

#endif
