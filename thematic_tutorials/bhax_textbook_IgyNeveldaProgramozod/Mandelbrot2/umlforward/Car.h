#include <exception>
using namespace std;

#ifndef __Car_h__
#define __Car_h__

#include "Vehicle.h"

// class Vehicle;
class Car;

class Car: public Vehicle
{
	protected: int _doorCount;
	protected: bool _isAutomatic;

	public: int getDoor();

	public: void setDoor(int aDoor);

	public: bool getAutomatic();

	public: void setAutomatic(bool aGearbox);

	public: void makeSound();
};

#endif
