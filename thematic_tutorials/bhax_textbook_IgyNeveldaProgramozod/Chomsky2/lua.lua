function para2D(fmts)
    	local sntnc = fmts:split(":")
        r=sntnc[1]
        fmts_shift = 1
        for i = 0, r-1 do
            fmts_shift = fmts_shift +1
            n=sntnc[fmts_shift]
            fmts_shift = fmts_shift +1
            nod=sntnc[fmts_shift]
            tex.sprint(
            		"\\tikzstyle{shifting}=[xshift=", i,"cm]"         			
					,"\\shade[shifting, left color=blue!8, right color=blue!90] (0,0) rectangle +(1,1);"
					,"\\draw[shifting, step=\\N{",n,"}cm, black] (0,0) grid (1,1);")		
            for j = 1, nod do
            	fmts_shift = fmts_shift +1
            	x=sntnc[fmts_shift]
            	fmts_shift = fmts_shift +1
            	y=sntnc[fmts_shift]+1
				tex.sprint(
					"\\node[shifting, fill=orange, anchor=north west,  inner sep=0pt,minimum size=\\N{",n,"}cm-\\pgflinewidth] at \\D{",n,"}{",x,"}{",y,"} {};")
            end
		end
	end

function para3D(direct, fmts)
    	dir=tonumber(direct)
        d={{2,1,2,-1,1,-1}, {1,0,1,-1,0,-1}}
       	local sntnc = fmts:split(":")
        r=sntnc[1]
        fmts_shift = 1
        for i = 0, r-1 do
            fmts_shift = fmts_shift +1
            n=sntnc[fmts_shift]
            fmts_shift = fmts_shift +1
            nod=sntnc[fmts_shift]
            tex.sprint(
            "\\tikzstyle{leftslant}=[yslant=-.5, xshift=",-1+i*d[dir][1],"cm, yshift=",-1+i*d[dir][2],"cm]"
			,"\\tikzstyle{rightslant}=[yslant=0.5, xshift=",i*d[dir][3],"cm, yshift=",-1+i*d[dir][4],"cm]"
			,"\\tikzstyle{topslant}=[yslant=0.5, xslant=-1, xshift=", i*d[dir][5],"cm, yshift=", i*d[dir][6],"cm]"
			,"\\shade[leftslant, right color=darkgreen!80,left color=black!100] (0,0) rectangle +(1,1);"
			,"\\draw[leftslant, step=\\N{",n,"}cm, black] (0,0) grid (1,1);") 
            for j = 1, nod do
            	fmts_shift = fmts_shift +1
            	x=sntnc[fmts_shift]
            	fmts_shift = fmts_shift +1
            	y=sntnc[fmts_shift]+1
            	tex.sprint(
            	"\\node[leftslant, fill=red, anchor=north west,  inner sep=0pt,minimum size=\\N{",n,"}cm-\\pgflinewidth] at \\D{",n,"}{",x,"}{",y,"-.5*",x,"} {};")
            end    
            fmts_shift = fmts_shift +1    
            n=sntnc[fmts_shift]
            fmts_shift = fmts_shift +1
	        nod=sntnc[fmts_shift]
			tex.sprint(
				"\\shade[rightslant, right color=green!20,left color=darkgreen!85] (0,0) rectangle +(1,1);",
				"\\draw[rightslant, step=\\N{",n,"}cm, ultralightblack](0,0) grid (1,1);")
			for j = 1, nod do
            	fmts_shift = fmts_shift +1
				x=sntnc[fmts_shift]
            	fmts_shift = fmts_shift +1
				y=sntnc[fmts_shift]
				tex.sprint(
					"\\node[rightslant, fill=red, anchor=south west,  inner sep=0pt,minimum size=\\N{",n,"}cm-\\pgflinewidth] at \\D{",n,"}{",x,"}{",y,"+.5*",x,"} {};")
            end    
            fmts_shift = fmts_shift +1
			n=sntnc[fmts_shift]
			fmts_shift = fmts_shift +1
            nod=sntnc[fmts_shift]		
			tex.sprint(
				"\\shade[topslant, left color=black!100,right color=green!20] (0,0) rectangle +(1,1);",
				"\\draw[topslant, step=\\N{",n,"}cm, lightblack] (0,0) grid (1,1);")
            for j = 1, nod do
           		fmts_shift = fmts_shift +1
            	x=sntnc[fmts_shift]
            	fmts_shift = fmts_shift +1
				y=sntnc[fmts_shift]	    
				tex.sprint(
					"\\node[topslant, fill=red, anchor=south west,  inner sep=0pt,minimum size=\\N{",n,"}cm-\\pgflinewidth] at \\D{",n,"}{",y,"-",x,"}{",tonumber(y)/2,"+.5*",x,"} {};")
           end    
	end
end

function N(n) 
            tex.sprint(1/n)
end 

function D(n, x, y) 
            tex.sprint("(",       (1/n)*x,     ", ",   (1/n)*y,       ")")
end 
