#include <iostream>
using namespace std;

int* h(int value)
{
    int* address=&value;
    return address;
}

int* g()
{
    int a=0;
    int* b=&a;
    return b;
}

//typedef int *(*l) ();

int main()
{
    int a=10;
    cout<<"1. "<<a<<"\n";

    int *b = &a;
    cout<<"2. "<<b<<"\n";

    int &r = a;
    cout<<"3. "<<r<<"\n";

    int c[5]={1,2,3,4,5};
    cout<<"4. ";
    for (int i=0;i<(sizeof(c)/sizeof(c[0]));i++)
        cout<<c[i]<<"\t";
    cout<<"\n";

    int (&tr)[5] = c;
    cout<<"5. ";
    for (int i=0;i<(sizeof(tr)/sizeof(tr[0]));i++)
        cout<<tr[i]<<"\t";
    cout<<"\n";

    int *d[5];
    cout<<"6. ";
    for (int i=0;i<(sizeof(d)/sizeof(d[0]));i++)
        d[i]=&tr[i];
    for (int i=0;i<(sizeof(d)/sizeof(d[0]));i++)
        cout<<d[i]<<"\t";
    cout<<"\n";

    int x=1;
    cout<<"7. ";
    int* address=h(x);
    cout<<address<<"\n";

    //l tmp=g;
    int *(*l) ();
    cout<<"8. ";
    l=g;
    int* f=l();
    cout<<(void*)l<<"\t"<<f<<"\n";

}
