#include <stdio.h>
#include <math.h>

void kiir (double tomb[], int db)
{
    for (int i=0; i<db; i++)
    {
        printf("%f\n",tomb[i]);   //a paraméterként kapott tömb elemeit,
    }                             //azaz a pagerank értékeket kiíratja.
}

double tavolsag(double PR[],double PRv[],int n)
{
    double osszeg = 0.0;
    for(int i=0;i<n;i++)
    {
        osszeg += (PRv[i] - PR[i]) * (PRv[i] - PR[i]);
    }

    return sqrt(osszeg);
}


int main()
{
    double L[4][4]={                       //ez a linkmátrixunk. Minden egyes oldalhoz tartozik 1 oszlop, és egy sor is. Ha i sor van és j oszlop, akkor
        {0.0, 0.0, 1.0/3.0, 0.0},          //akkor a mátrix minden elemét úgy kapjuk meg, hogy: ha a j. oldal hivatkozik az i. oldalra, akkor az érték
        {1.0, 1.0/2.0, 1.0/3.0, 0.0},      //1/(a j. oldalról kimenő összes link száma); egyébként az érték 0.  
        {0.0, 1.0/2.0, 0.0, 0.0},          
        {0.0, 0.0, 1.0/3.0, 1.0}
    };
    double PR[4]={0.0, 0.0, 0.0, 0.0};     //ebbe a tömbbe fogjuk kiszámítani a keresett PageRank értékeket.
    double PRv[4]={1.0/4.0, 1.0/4.0, 1.0/4.0, 1.0/4.0};    //a kezdeti PageRank értékek: 4 oldal van, a PageRank értékek összege mindig 1, ezért egyenlően osztjuk el
                                                           //az értékeket: minden oldalnak 0.25 jut.
    for (int i=0; i<100; i++)              //a külső ciklus azért kell, mert minél többször végezzük el a számítást, annál
    {                                      //pontosabb értéket fogunk kapni a PageRankre, konvergálni fog egy számhoz.
        for (int j=0; j<4; j++)
        {
            PR[j]=0.0;                     //a belső ciklus lefutása előtt minden sorhoz tartozó PageRank értéket le kell nullázni, különben a program lefutása után nagyon nagy
            for (int k=0; k<4; k++)        //értékeket kapnánk, hiszen PR[j]-hez mindig hozzáadjuk az előző értékét is.
            {
                PR[j]=PR[j]+(L[j][k]*PRv[k]);       //a mátrix soraiból kapjuk meg az adott sorbeli oldal PageRank-jét: a linkmátrix ugyanezen sorában minden oszlop értékét 
            }                                       //megszorozzuk a megfelelő PageRank-ekkel.
        }

	if ( tavolsag(PR,PRv, 4) < 0.00001)
            break;

        for (int l=0; l<4; l++)
        {
            PRv[l]=PR[l];           //a kiszámított PageRank-eket eltároljuk a PRv[] vektorban, hogy a ciklus következő
        }                           //lefutásakor a módosított értékekkel dolgozzunk.
    }

    kiir(PR, 4);           //kiíratjuk az eredményt.

    return 0;
}
