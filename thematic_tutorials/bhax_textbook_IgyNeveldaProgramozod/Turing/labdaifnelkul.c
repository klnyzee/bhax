#include <stdio.h>
#include <stdlib.h>
#include <curses.h>
#include <unistd.h>

int main()
{
    WINDOW *ablak;
    ablak = initscr ();
    int x = 1;
    int y = 1;
    int xdiff = 1;
    int ydiff = 1;
    int width;
    int height;
    for (;;)
    {
        getmaxyx ( ablak, height , width );
        int szelesseg[width];
        int magassag[height];
        szelesseg[0]=-1;
        magassag[0]=-1;
        szelesseg[width-1]=-1;
        magassag[height-1]=-1;
        for (int k=1;k<width-1;k++)
        {
            szelesseg[k]=1;
        }
        for (int k=1;k<height-1;k++)
        {
            magassag[k]=1;
        }
        mvprintw ( y, x, "O" );
        refresh ();
        usleep ( 100000 );
        xdiff = xdiff * szelesseg[x];
        ydiff = ydiff * magassag[y];
        x = x + xdiff;
        y = y + ydiff;
    }
}
