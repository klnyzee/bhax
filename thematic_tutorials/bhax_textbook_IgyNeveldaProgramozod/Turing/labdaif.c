#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int
main ( void )
{
    WINDOW *ablak;
    ablak = initscr ();
    int x = 0;
    int y = 0;
    int xdiff = 1;
    int ydiff = 1;
    int width;
    int height;
    for (;;)
    {
        getmaxyx ( ablak, height , width );
        mvprintw ( y, x, "O" );
        refresh ();
	clear();
        usleep ( 100000 );
        x = x + xdiff;
        y = y + ydiff;
        if (x>=width-1 || x<=0)
        {
            xdiff=xdiff*-1;
        }
        if (y<=0 || y>=height-1)
        {
            ydiff=ydiff*-1;
        }
    }
    return 0;
}
