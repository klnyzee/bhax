#include<stdio.h>
int main()
{
    //2 változó cseréje szorzással és osztással
    double a=12.4, b=5.37;
    printf("Kezdeti értékek: a=%f, b=%f\n",a,b);
    b=a*b;
    a=b/a;
    b=b/a;
    printf("Felcserélt értékek: a=%f, b=%f\n",a,b);
    return 0;
}
