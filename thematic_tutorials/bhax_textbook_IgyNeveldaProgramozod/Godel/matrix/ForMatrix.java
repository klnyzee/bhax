public class ForMatrix extends AbstractMatrix {

	public ForMatrix(int[][] matrix) {
		super(matrix);
	}

	public ForMatrix(int rowsLength, int columnsLength) {
		super(rowsLength, columnsLength);
	}

	public Matrix multiply(AbstractMatrix input) {
		int[][] result = new int[this.rowsLength][input.columnsLength];

		for (int row = 0; row < result.length; row++) {
			for (int col = 0; col < result[row].length; col++) {
				result[row][col] = multiplyMatricesCell(this.matrix, input.matrix, row, col);
			}
		}

		return new ForMatrix(result);
	}

	private int multiplyMatricesCell(int[][] firstMatrix, int[][] secondMatrix, int row, int col) {
		int cell = 0;
		for (int i = 0; i < secondMatrix.length; i++) {
			cell += firstMatrix[row][i] * secondMatrix[i][col];
		}
		return cell;
	}

}
