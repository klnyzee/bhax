import java.util.Arrays;

public abstract class AbstractMatrix implements Matrix {

	protected final int[][] matrix;
	protected final int rowsLength;      //sorszám
	protected final int columnsLength;   //oszlopszám

	public AbstractMatrix(int[][] matrix) {
		this.matrix = matrix;
		this.rowsLength = matrix.length;
		this.columnsLength = matrix[0].length;
	}

	public AbstractMatrix(int rowsLength, int columnsLength) {
		this.matrix = new int[rowsLength][columnsLength];
		this.rowsLength = rowsLength;
		this.columnsLength = columnsLength;
	}

	@Override
	public void setElement(int x, int y, int value) {
		matrix[x][y] = value;
	}

	@Override
	public Matrix multiply(Matrix input) {
		if (input instanceof AbstractMatrix) {
			return multiply((AbstractMatrix) input);
		}
		throw new IllegalArgumentException("The input matrix should be an instance of AbstractMatrix");
	}

	abstract protected Matrix multiply(AbstractMatrix input);

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + columnsLength;
		result = prime * result + Arrays.deepHashCode(matrix);
		result = prime * result + rowsLength;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof AbstractMatrix)) {
			return false;
		}
		AbstractMatrix other = (AbstractMatrix) obj;
		if (columnsLength != other.columnsLength) {
			return false;
		}
		if (!Arrays.deepEquals(matrix, other.matrix)) {
			return false;
		}
		if (rowsLength != other.rowsLength) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Matrix [matrix=" + Arrays.toString(matrix) + ", rowsLength=" + rowsLength + ", columnsLength="
				+ columnsLength + "]";
	}

}
