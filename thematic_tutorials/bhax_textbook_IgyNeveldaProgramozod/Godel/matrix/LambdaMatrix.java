import java.util.Arrays;
import java.util.function.Function;
import java.util.function.IntUnaryOperator;
import java.util.stream.IntStream;

public class LambdaMatrix extends AbstractMatrix {

	public LambdaMatrix(int[][] matrix) {
		super(matrix);
	}

	public LambdaMatrix(int rowsLength, int columnsLength) {
		super(rowsLength, columnsLength);
	}

	@Override
	protected LambdaMatrix multiply(AbstractMatrix input) {
		int[][] result = Arrays.stream(this.matrix)
				.map(r -> IntStream.range(0, input.columnsLength)
						.map(i -> IntStream.range(0, input.rowsLength).map(j -> r[j] * input.matrix[j][i]).sum())
						.toArray())
				.toArray(int[][]::new);

		return new LambdaMatrix(result);
	}

	public static void main(String[] args) {
		LambdaMatrix matrixA = new LambdaMatrix(new int[][] {{1, 5},
				{2, 3},
				{1, 7}});
		LambdaMatrix matrixB = new LambdaMatrix(new int[][] {{1, 2, 3, 7},
				{5, 2, 8, 1}});
		LambdaMatrix expected = new LambdaMatrix(new int[][] {{26, 12, 43, 12},
				{17, 10, 30, 17},
				{36, 16, 59, 14}});

		for (int i=0; i<expected.rowsLength; i++)
		{
			for (int j=0; j<expected.columnsLength; j++)
			{
				System.out.print(expected.matrix[i][j] + "\t");
			}
			System.out.print("\n");
		}
		System.out.print("\n");

		LambdaMatrix actual = matrixA.multiply(matrixB);

		for (int i=0; i<actual.rowsLength; i++)
		{
			for (int j=0; j<actual.columnsLength; j++)
			{
				System.out.print(actual.matrix[i][j] + "\t");
			}
			System.out.print("\n");
		}
	}
}
