import java.util.ArrayList;
import java.util.LinkedList;
import java.util.*;

public class Measure {

    public static class TimeTest {

        public static void addTest(ArrayList<Integer> arr, int index) {
            long start = System.nanoTime();
            arr.add(index, new Random().nextInt());
            long end = System.nanoTime();
            System.out.println("Arraylist: " + (end - start));
        }

        public static void addTest(LinkedList<Integer> link, int index) {
            long start = System.nanoTime();
            link.add(index, new Random().nextInt());
            long end = System.nanoTime();
            System.out.println("Linkedlist: " + (end - start));
        }

        public static void removeTest(ArrayList<Integer> arr, int index) {
            long start = System.nanoTime();
            arr.remove(index);
            long end = System.nanoTime();
            System.out.println("Arraylist: " + (end - start));
        }

        public static void removeTest(LinkedList<Integer> link, int index) {
            long start = System.nanoTime();
            link.remove(index);
            long end = System.nanoTime();
            System.out.println("Linkedlist: " + (end - start));
        }

        public static void getTest(ArrayList<Integer> arr, int index) {
            long start = System.nanoTime();
            arr.get(index);
            long end = System.nanoTime();
            System.out.println("Arraylist: " + (end - start));
        }

        public static void getTest(LinkedList<Integer> link, int index) {
            long start = System.nanoTime();
            link.get(index);
            long end = System.nanoTime();
            System.out.println("Linkedlist: " + (end - start));
        }
    }

    public static void main(String[] args) {
        ArrayList<Integer> arr = new ArrayList<Integer>();
        LinkedList<Integer> link = new LinkedList<Integer>();

        for (int i=0; i<100000; i++) {
            arr.add(i);
            link.add(i);
        }

        System.out.println("\n-----Add operation (lower index) time test-----");
        TimeTest.addTest(arr, 20);
        TimeTest.addTest(link, 20);

        System.out.println("\n-----Add operation (higher index) time test-----");
        TimeTest.addTest(arr, 60000);
        TimeTest.addTest(link, 60000);

        System.out.println("\n-----Remove operation (lower index) time test-----");
        TimeTest.removeTest(arr, 20);
        TimeTest.removeTest(link, 20);

        System.out.println("\n-----Remove operation (higher index) time test-----");
        TimeTest.removeTest(arr, 60000);
        TimeTest.removeTest(link, 60000);

        System.out.println("\n-----Get operation (lower index) time test-----");
        TimeTest.getTest(arr, 20);
        TimeTest.getTest(link, 20);

        System.out.println("\n-----Get operation (higher index) time test-----");
        TimeTest.getTest(arr, 60000);
        TimeTest.getTest(link, 60000);
    }
}
