import java.util.Random;

public class LZWfa {

    public class BinRandTree {

        protected class Node {
                
            private int value;
            private Node left;
            private Node right;

            public Node(int value) {
                this.value=value;
                left=null;
                right=null;
            }

            public int getValue() {
                return value;
            }    

            public Node leftChild() {
                return left;
            }

            public Node rightChild() {
                return right;
            }

            public void leftChild(Node node) {
                left = node;
            }

            public void rightChild(Node node) {
                right = node;
            }
        }

        Node root=null;
        Node current=root;
        int depth=0;

        public void add(int value) {
            root=addMember(root, value);
        }

        public Node addMember(Node actual, int value) {
            Random rand = new Random();
            int randomNum = rand.nextInt(10) + 1;
            if (actual==null) {
                return new Node(value);
            }
            else if (actual.getValue()==value) {
                System.out.println("Már szerepel ilyen szám a fában.");
            }
            else if (randomNum==1) {
                actual=root;
                this.addMember(actual, value);
            }
            else if (randomNum%2==0) {
                actual.leftChild(addMember(actual.left, value));
            }
            else {
                actual.rightChild(addMember(actual.right, value));
            }
            return actual;
        }

        public void print() {
            printr(root);
        }

        public void printr(Node node) {
            if(node!=null)
            {
        
                depth=depth+1;
                printr(node.left);
        
                for(int i=0; i<depth; i++)
                    System.out.print("---");            
                System.out.print(node.getValue()+" "+depth+" \n");     
        
                printr(node.right);
                depth=depth-1;
        
            }
        }
    }

    public class BinSearchTree extends BinRandTree {

        public Node addMember(Node actual, int value) {
            if (actual==null) {
                return new Node(value);
            }
            else if (actual.getValue()==value) {
                System.out.println("Már szerepel ilyen szám a fában.");
            }
            else if (actual.getValue()>value) {    
                actual.leftChild(addMember(actual.leftChild(), value));
            }
            else {
                actual.rightChild(addMember(actual.rightChild(), value));
            }
            return actual;
        }
    }

    public class LZWTree extends BinRandTree {
        
        public LZWTree() {
            root=new Node(0);
            current=root;
        }

        public void add(int value) {
            if (value==0) {          
                if(current.leftChild()==null) {
                    current.leftChild(new Node(value));
                    current=root;
                } 
                else {           
                    current=current.leftChild();
                }            }
            else {
                if(current.rightChild()==null) {
                    current.rightChild(new Node(value));
                    current=root;
                }
                else {           
                    current=current.rightChild();
                }
            }
        }
    }

    public static void main(String[] args) {
        LZWfa tree=new LZWfa();
        LZWfa.BinRandTree rand=tree.new BinRandTree();
        rand.add(15);
        rand.add(22);
        rand.add(75);
        rand.add(4);
        rand.add(9);
        rand.add(99);
        rand.print();
        System.out.println(" ");
        LZWfa.BinSearchTree search=tree.new BinSearchTree();
        search.add(20);
        search.add(50);
        search.add(55);
        search.add(14);
        search.add(3);
        search.add(9);
        search.add(5);
        search.add(36);
        search.print();
        System.out.println(" ");
        LZWfa.LZWTree lzw=tree.new LZWTree();
        lzw.add(0);
        lzw.add(1);
        lzw.add(0);
        lzw.add(1);
        lzw.add(0);
        lzw.add(0);
        lzw.add(0);
        lzw.add(1);
        lzw.add(1);
        lzw.add(0);
        lzw.add(1);
        lzw.add(1);
        lzw.add(1);
        lzw.add(1);
        lzw.add(0);
        lzw.print();
    }

}
