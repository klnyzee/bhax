class Vehicle {
	public void openDoor () {
		System.out.println("Doors opened.");
	}
}

class Car extends Vehicle {
}

class Scooter extends Vehicle {
}

class Liskovviolates {
	public static void function(Vehicle vehicle) {
		vehicle.openDoor();
	}

	public static void main(String[] args) {
		Vehicle vehicle = new Vehicle();
		function(vehicle);
		Car car = new Car();
		function(car);
		Scooter scooter = new Scooter();
		function(scooter);
	}
}
