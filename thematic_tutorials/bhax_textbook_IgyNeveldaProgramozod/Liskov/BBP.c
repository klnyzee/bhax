#include <stdio.h>
#include <string.h>
#include <math.h>

long modulo(int n, int k) {
        int t=1; //2^0=1
        while (t<=n) {
            t=t*2;
        }
        long r=1;
        int b=16;
        while (1) {
            if (n>=t) {
                r=(b*r)%k;
                n=n-t;
            }
            t=t/2;
            if (t>=1) {
                r=(r*r)%k;
            }
            if (t<1) {
                break;
            }
        }

        return r;
    }

double algorithm(int digit, int j) {
        double si=0.0;
        for (int k=0;k<=digit;k++) {
            si+=(double)modulo(digit-k,8*k+j)/(double)(8*k+j);
        }
        for (int k=digit+1;k<=2*digit;k++) {
            si+=pow(16,digit-k)/(double)(8*k+j);
        }
        
        si=si-(long)si;
        return si;
}

void piDigits(int digit) {
   	double s1;
    	double s4;
   	double s5;
   	double s6;
   	double pi;
   	char codeTable[100]="0123456789ABCDEF";
	char hex[100]="";
	double index;
	int i=0;

	s1=algorithm(digit, 1);
	s4=algorithm(digit, 4);
	s5=algorithm(digit, 5);
	s6=algorithm(digit, 6);
	pi=(4*s1-2*s4-s5-s6)-(long)(4*s1-2*s4-s5-s6);
	if (pi<0) {
		pi=pi+1;	
	}

	while (pi!=0) {
		index=pi*16-((pi*16)-(long)(pi*16));
		hex[i]=codeTable[(int)index];
		pi=(pi*16)-(long)(pi*16);
		i+=1;
	}

	printf("%s\n",hex);
}

int main() {
	//printf("How many digits? ");
        //long int digits;
	//scanf("%li",&digits);
        //piDigits(digits);
        piDigits(1000000);
}
