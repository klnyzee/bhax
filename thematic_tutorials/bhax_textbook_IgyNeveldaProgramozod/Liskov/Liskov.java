class Vehicle {
}

class VehicleWithDoors extends Vehicle {
	public void openDoor () {
		System.out.println("Doors opened.");
	}
}

class Car extends VehicleWithDoors {
}

class Scooter extends Vehicle {
}

class Liskov {
	public static void function(VehicleWithDoors vehicle) {
		vehicle.openDoor();
	}

	public static void main(String[] args) {
		Vehicle vehicle = new Vehicle();
		//function(vehicle);
		Car car = new Car();
		function(car);
		Scooter scooter = new Scooter();
		//function(scooter);
	}
}
