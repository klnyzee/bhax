#include <iostream>

class Vehicle {
};

class VehicleWithDoors : public Vehicle {
	public:
		void openDoor () {
			std::cout<<"Doors opened.\n";
		}
	};

class Car : public VehicleWithDoors {
};

class Scooter : public Vehicle {
};

class Liskov {
	public:
		void function(VehicleWithDoors& vehicle) {
			vehicle.openDoor();
		}
};

int main() {
	Liskov program;
	Car car;
	program.function(car);
	Scooter scooter;
	//program.function(scooter);
}
