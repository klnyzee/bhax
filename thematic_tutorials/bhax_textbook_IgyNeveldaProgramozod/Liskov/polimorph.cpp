#include <iostream>

class Dog {
	public:
		void makeSound() {
			std::cout<<"Woof! Bark!\n";
		}
};

class GermanShepherd : public Dog {
	public:
		void Guard () {}    //a német juhászok jó őrző védő kutyák
};


int main() {
	GermanShepherd german1;
	
	german1.makeSound();  //ezekkel nincsen baj, hiszen a "german1"
	german1.Guard();      //referencia Gyermekosztály típusú, tehát
	                      //megörökölte a makeSound() metódust is

	Dog german2;

	german2.makeSound();  //ezzel már baj van, mivel a "german2"
	german2.Guard();      //Ősosztály típusú referencia, azaz csak 
	                      //a makeSound() metódust érjük el
}
